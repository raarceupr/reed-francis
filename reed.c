//
// Warning this program is "hardcoded" for fields up to 2^8!
// 

//
// Usage:
// ./reed {num of vars} {w, where field is 2^w} {value of B1}
//
// Example:
// ./reed 5 7 1 
// Computes the number of (1,B2,B3) that have at least one solution.
//
// Example:
// ./reed 5 7 1 2 9 65 
// Computes the number of (1,B2,B3) that have at least one solution for 
// the equations:
// x1    + x2    + x3    + x4    + x5     = 1
// x1^9  + x2^9  + x3^9  + x4^9  + x5^9   = B2
// x1^65 + x2^65 + x3^65 + x4^65 + x5^65  = B3

#include <stdio.h>
#include <stdlib.h>
#include "galois.h"

typedef unsigned short uc;
typedef unsigned int uint;
//
// Given a number count the number of ones in its 
// binary representation.
//
uc countOnes(uc n) {
	uc ctr = 0;
	while ( n ) {
		ctr = ctr + (n & 0x1);
		n = n >> 1;
	}
	return ctr;
}

//
// Given a vector of ucs and its size, 
// prints to the stdout.
//
void print_uc_vector(const uc *a, int size){
  int i;
  for (i = 0; i < size; i++) {
	printf("%u ", a[i]);
  }
  printf("\n");
}


//
// Params:
//   a: vector that contains a combination 
//   desiredResult: desired result of the xor between x1...xn
//   evensAndOdds: the array that contains a list of all 
//       numbers from [0,2^n-1] that have even, and odd number of ones.
//   b: result array
//   size: number of elements
//
void buildPreTranspose(uc *a, uc desiredResult, uc *b, uc **evensAndOdds, int size) {
	uc mask = 0x1;
	int i;
	for (i = size - 1; i >=0 ; i--) {
		b[i] = evensAndOdds[desiredResult & mask][a[i]];
		desiredResult = desiredResult >> 1;
	}

}

//
// Builds the lookup tables from alpha to polynomial (A2P), and
// polynomial to alpha (P2A), for a field 2^w.
// 
// For example: 
// A2P[0] = 1, means that alpha^0 = 1
// P2A[4] = 2, means that x^2 = alpha^2 (becuase the polynomial 0100 is x^2)
//
void buildTables(uc *A2P, uc *P2A, uc w) {
  int i; 

  for (i = 0; i < (1 << w) - 1; i++) {
  	*A2P = galois_ilog(i, w);
 	*P2A = galois_log(i, w);
  	A2P++; P2A++; 
  }	
}


// Buils the powerTables for the first powerQty odd numbers
// 
// For example, if powerqty = 2, it will buils tables for the 
// powers of 3 and 5.
void buildPowerTables(const uc *A2P, const uc *P2A, uc **PTable, 
	uc *powers, int powerQty, uc w) {
  int i, p;
  uc currPower; 
  for (p = 0; p < powerQty; p++) {
  	PTable[p] = malloc(sizeof(uc) * (1 << w));
  	currPower = powers[p];
	for (i = 0; i < (1 << w) - 1; i++) {
		// int e = ( (2 * p + 3 ) * i ) % ( (1 << w) - 1);
		int e = ( currPower * i ) % ( (1 << w) - 1);
		PTable[p][A2P[i]] = A2P[e];
	}	
  }
}


// Given an array numbers, builds its binary transpose.
// a: input array
// b: output array
// n: the input array contains w numbers in the range [0,2^n-1]
//
// The output output array will consist of n numbers in the 
// range [0,2^w]
//
// For example, if the input array contains (7, 1, 2)
// The output:
//   0 0 0  = 0
//   1 0 0  = 4
//   1 0 1  = 5
//   1 1 0  = 6 
//   x1,x2,x3,x4 = 0,4,5,6

void myTranspose(uc *a, uc *b, uc n, uc w) {
	char i = 0;
	for (i = 0; i < n ; i++) b[i] = 0;
	for (i = 0; i < w ; i++) {
		uc currA = a[i];
		
		int j;
		for (j = n-1; j >= 0; j--) {
			//printf("%d %d currA: %u\n", i, j,  currA);
		  b[j] = b[j] << 1;
		  b[j] = b[j] | (currA & 0x1);
		  currA = currA >> 1;

		}
	}
}


unsigned char *gCheckBox;
unsigned long long gCtr;

// Given the array 

void computeRemainingEq(const uc *transpose, int n, uc **PTable, int powerQty, uc w) {
	int i,j;
	uc total;
	unsigned int index = 0x0;

	//printf("Power results: ");

	for (i = 0; i < powerQty; i++) {
		total = 0;
		for (j = 0; j < n; j++) {
			total ^= PTable[i][transpose[j]];
			//printf("(%d, %d) table: %x  Total %x\n",  i, j, PTable[i][transpose[j]], total);
		}
		//printf("Total of power %d is %x\n", (2*i + 3), total);
		//printf("0x%.2x " ,total);
		index = index << w;
		index = index | total;
	}
	// printf("w = %d\n",w);
	if (gCheckBox[index] == 0) {
		gCheckBox[index] = 1;
		gCtr++;
        if (gCtr % 100000 == 0) { 
		printf("gCtr: %llu   %d %d  ", gCtr, index >> 8, index & 0xff );
		print_uc_vector(transpose,n);
        } 

		if (gCtr == (unsigned long long)1 << (w*powerQty)) {
			 printf("All solutions found!!\n");
			exit(0);	
		}
	}
}

// n: the number of vars
// w: 2^w is the size of the field
void everyCombi(int n, uc fieldSize, uc **evensAndOdds,  uc **PTable, int powerQty, uc w , uc b1) {
	//uc w = 1 << (n-1);
	fieldSize = fieldSize >> 1;
	fieldSize = 1 << (n-1);
	uc *a = malloc(n * sizeof(uc));
	uc *preTranspose = malloc(n * sizeof(uc));
	uc *transpose = malloc(n * sizeof(uc));
	
	int i;


	

	unsigned long long int ctr = 0;
	for (i = 0; i < n; i++) a[i] = 0;

	// i = n-1;
		i = w - 1;

	int dir = 0;
	while (i >= 0  ) {

		if (dir == 0) {
			if (a[i] <= fieldSize -1) { 

				ctr++;
				 if (ctr % 1000000 == 0) { 
					//printf("allCtr: %llu  ", ctr);
					//print_uc_vector(a,w); 

					  a[0] = rand() % fieldSize;
					  a[1] = rand() % fieldSize;
					  a[2] = rand() % fieldSize;
					  //a[n-2] = rand() % fieldSize;
					  //a[n-3] = rand() % fieldSize;
				}
					//transpose(a,b,w)
				//printf("i: %d ", i);
				//printf("%llx combi: ", ctr);
				//print_uc_vector(a,w);

				buildPreTranspose(a, b1, preTranspose, evensAndOdds, w);
				//printf("preTranspose: ");
				//print_uc_vector(preTranspose,w);

				myTranspose(preTranspose,transpose,n,w);

				//printf("transpose: ");
				//print_uc_vector(transpose,n);

				computeRemainingEq(transpose,n, PTable, powerQty,w);
								a[i]++; 

				// }
			}
			else {
				a[i] = 0;
				dir = -1;
				i--;
			}
		}
		else if (dir == -1) {
			if (a[i] < fieldSize- 1) { 
				a[i]++;
				dir = +1;
				i++;
			} 
			else {
				a[i] = 0;
				i--;
			}
		}
		else {
			if (i < w - 1) i++;
			else { dir = 0; 
				//print_uc_vector(a,n);
			}
		}
        if (i < 0) { 
			for (i = 3; i < n; i++) a[i] = 0;
                      a[0] = rand() % fieldSize;
                      a[1] = rand() % fieldSize;
                      a[2] = rand() % fieldSize;
        }
	}
}



uint gpow(uint a, uint b, uint w) {
	uint res = 0x1;
	int i;
	for (i = 0; i < b; i++) res = galois_single_multiply(a,res,w); 
	return res;
}

void OLDvalidatePowerTable(uc **PTable, int w) {
	int i;
	for (i = 0; i < (1 << w); i++ ) {
		uc expected3 = galois_single_multiply(i, galois_single_multiply(i, i, w),w);
		if ( PTable[0][i] != expected3 ) { 
			printf("Validate power3 of %d, expected %d, got %d\n", i, expected3, PTable[0][i]);
			exit(1);
		}
		uc expected5 = galois_single_multiply(i, galois_single_multiply(i, expected3, w),w);
		if ( PTable[1][i] != expected5 ) exit(1);
	}
}

void validatePowerTable(uc **PTable, int w, uc *powers, uc powerQty) {
	int i, p;
	uc expected;
	for (p = 0; p < powerQty; p++) {

		for (i = 0; i < (1 << w); i++ ) {
			expected = gpow(i, powers[p], w); 
			if ( PTable[p][i] != expected) { 
				printf("Validate %dth power of %d, expected %d, got %d\n", 
					powers[p],i, expected, PTable[p][i]);
				exit(1);
			}
		}
	}
}


int main(int argc, char **argv)
{
  unsigned int x, w, b1;
  int i,j;
  srand(time(NULL));
  gCtr = 0;

  uc *powers;
  uc numPowers;

  if (argc == 4) {
  	powers = malloc(2*sizeof(uc));
    powers[0] = 3; 
    powers[1] = 5;
    numPowers = 2;
  }
  else if (argc > 4) {
  	numPowers = atoi(argv[4]);
  	if (argc < 4 + numPowers)  {
  	  fprintf(stderr, "Please provide %d powers (%d provided)\n",
  	   				   numPowers, argc-5 );
      exit(1);
  	}

  	powers = malloc(numPowers*sizeof(uc));
  	for (i = 0; i < numPowers; i++) 
  		powers[i] = atoi(argv[5 + i]);
  	printf("Powers are: ");
  	for (i = 0; i < numPowers; i++) 
  		printf("%d\n", powers[i]); 	
  }

  else {
    fprintf(stderr, "usage: gf_log x w B1- returns the discrete log if x in GF(2^w)\n");
    exit(1);
  }

  sscanf(argv[1], "%u", &x);
  w = atoi(argv[2]);
  b1 = atoi(argv[3]);

  if (w < 1 || w > 32) { fprintf(stderr, "Bad w\n"); exit(1); }

  //if (x == 0 || (w < 32 && x >= (1 << w))) { fprintf(stderr, "x must be in [1,%d]\n", (1 << w)-1); exit(1); }

 
  //int powerQty = 2;

  // Tablas de conversion

  uc *A2P = malloc(sizeof(uc) * (1 << w));
  uc *P2A = malloc(sizeof(uc) * (1 << w));
  buildTables(A2P,P2A, w);

  uc **PTable = malloc(numPowers * sizeof(uc *)); 

  // buildPowerTables(A2P,P2A, PTable, 2, w) ;

  buildPowerTables(A2P,P2A, PTable, powers, numPowers, w) ;


  // uc *c = malloc(1 << w);
  uc *tmp;

  // for (i = 0; i < (1 << w) - 1; i++) {
  // 	*tmp = galois_ilog(i, w);
  // 	tmp++;
  // }

  printf("====From alpha to poly===\n"); 
  tmp = A2P;
  for (i = 0; i < (1 << w) - 1; i++, tmp++) 
  	printf("%u %u\n", i, (unsigned int)*tmp);

  printf("====From poly to alpha===========\n");
  tmp = P2A;
  for (i = 0; i < (1 << w) - 1; i++, tmp++) 
  	printf("%u %u\n", i, (unsigned int)*tmp);


  for (i = 0; i < numPowers; i++) {
  	printf("====Power of %d table===========\n", powers[i]);
	for (j = 0; j < (1 << w) - 1; j++) 
	  printf("%u %u\n", j, (unsigned int)PTable[i][j]);
  }

  validatePowerTable(PTable, w, powers, numPowers);
 unsigned long long int ii = 1;
  gCheckBox = malloc((ii<<(numPowers*w)));
  if (gCheckBox == 0) { 
    printf("not enough heap?\n");
    exit(1);
  }
  for (i = 0; i < (1<<numPowers*w) ; i++) gCheckBox[0] = 0;

  // gCheckBox = malloc(sizeof(uc *) * (1<<w));
  // for (i = 0; i < (1 << w) ; i++) {
  //    gCheckBox[i] = malloc( (1 << w) );
  //    for (j = 0; j < (1 << w) ; j++) {
  //    	gCheckBox[i][j] = 0;
  //    }
  // }


  // Classify the numbers into the ones that have an odd / even
  // quantity of 1's 

  uc **X = malloc(sizeof(uc *) * 2);
  X[0] = malloc(sizeof(uc) * (1 << (x-1)) );
  X[1] = malloc(sizeof(uc) * (1 << (x-1)) );
  uc *tmp0, *tmp1;
  tmp0 = X[0]; tmp1 = X[1];
  for (i = 0; i < 1 << x; i++) {
    if (countOnes(i) % 2 == 0) {*tmp0 = i; tmp0++;}
    else {*tmp1 = i; tmp1++;}
  } 

  for (i = 0; i < 1 << (x-1); i++) 
    printf("%u  ", X[0][i]);
  printf("\n");

  for (i = 0; i < 1 << (x-1); i++) 
    printf("%u  ", X[1][i]);
  printf("\n");
  


  	printf("and now every comb.....\n");
  	printf("number of vars: %d\n", x);
  everyCombi(x,1 << w,X, PTable, numPowers, w , b1);
  printf("gCtr: %llu\n", gCtr);
  exit(0);
}
