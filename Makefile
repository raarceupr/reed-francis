all: reed verify

reed: reed.c galois.c
	gcc -O4 -o reed reed.c galois.c

verify: verify.c galois.c
	gcc -o verify verify.c galois.c


clean: 
	rm reed verify
