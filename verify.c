// Sample call
// verify 5 8 161 2 9 65 n=5_f=8_d1=09_d2=65/n=5_f=7_d1=09_d2=65_B1=161.txt | tail
// verify that the file contains valid results for 
// n = 5, f = 8, B1 = 161, and the powers 9 and 65


#include <stdio.h>
#include <string.h>
#include "galois.h"

typedef unsigned char uc;


void print_uc_vector(const uc *a, int size){
  int i;
  for (i = 0; i < size; i++) {
	printf("%u ", a[i]);
  }
  printf("\n");
}




typedef unsigned int uint;

uint gpow(uint a, uint b, uint w) {
	uint res = 0x1;
	int i;
	for (i = 0; i < b; i++) res = galois_single_multiply(a,res,w); 
	return res;
}

uint gpowSum(uint *a, uint b, uint size, uint w) {
	uint res = 0;
	int i = 0;
	for (i = 0; i < size; i++) res = res ^ gpow(a[i],b,w);
	return res;
}

int main(int argc, char **argv)
{
  unsigned int x, w, b1;
  int i;
  uc numPowers;
  uc *powers;


  if (argc > 4) {
  	numPowers = atoi(argv[4]);
  	if (argc < 4 + numPowers + 1)  {
  	  fprintf(stderr, "Please provide %d powers (%d provided)\n",
  	   				   numPowers, argc-6 );
      exit(1);
  	}

  	powers = malloc(numPowers*sizeof(uc));
  	for (i = 0; i < numPowers; i++) 
  		powers[i] = atoi(argv[5 + i]);
  	printf("Powers are: ");
  	for (i = 0; i < numPowers; i++) 
  		printf("%d\n", powers[i]); 	
  }

  else {
    fprintf(stderr, "usage: gf_log x w B1- returns the discrete log if x in GF(2^w)\n");
    exit(1);
  }

  char fName[40];
  strcpy(fName, argv[5 + numPowers]);
  printf("Reading file: %s\n", fName);

  sscanf(argv[1], "%u", &x);
  w = atoi(argv[2]);
  b1 = atoi(argv[3]);

  FILE *f = fopen(fName,"r");
  if (!f) {
  	printf("File not found\n");
  	exit(1);
  } 
  char line[80];
  char tok[10];
  int h;
  uint x1, x2, x3, x4, x5, x6;
  uint p3, p5;

  uint *X = malloc(sizeof(uint)*x);
  uint *B = malloc(sizeof(uint)*numPowers);


  char **fmtString;
  fmtString = malloc(8*sizeof(char *));
  for (i = 0; i < 8; i++) fmtString[i] = malloc(80);
  fmtString[5] = "%*s %*s %u %u %u %u %u %u %u";
  fmtString[6] = "%*s %*s %u %u %u %u %u %u %u %u";
  fmtString[7] = "%*s %*s %u %u %u %u %u %u %u %u %u";
  fmtString[8] = "%*s %*s %u %u %u %u %u %u %u %u %u %u";



  while(fgets(line, 80, f) != NULL) {
   	printf("line: %s", line);

   	if (x == 5)
   	  sscanf (line, fmtString[5], 
	 	     &B[0], &B[1], &X[0], &X[1], &X[2], &X[3], &X[4]);
	 // /* get a line, up to 80 chars from fr.  done if NULL */
	 // sscanf (line, "%*s %*s %u %u %u %u %u %u %u %u", 
	 // 	&p3, &p5, &x1, &x2, &x3, &x4, &x5, &x6);
	 /* convert the string to a long int */
	  printf ("Read Values: B[0]: %u B[1]:%u %u %u %u %u %u\n", B[0], B[1], X[0], X[1], X[2], X[3], X[4]);

      uint expected = gpowSum(X, 1, x, w); //x1 ^x2 ^ x3 ^x4 ^ x5 ^ x6;
      if (expected != b1 ) { 
        printf("error in sum of power 1!!, expected %x got %x\n", 
     		expected, b1); 
     	exit(1);
      }

      int p;
      for (p = 0; p < numPowers; p++) {
      	printf("Checking power: %d \n", powers[p]);
      	expected = gpowSum(X, powers[p], x, w);
	    printf("Power %d: Expected %d\n", 
	     		powers[p],B[p]);       	
      	if (expected != B[p] ) { 
	        printf("error in sum of power %d!!, expected %d got %d\n", 
	     		powers[p],expected, B[p]); 
	     	exit(1);
        }
  	  }

   }
   printf("Validation complete!!!!\n");

  return 0;
}
